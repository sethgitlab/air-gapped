# air-gapped

Project with notes about setting up airgapped


Setup local images of security analyzers

```sh
# Setup your local docker registry
docker run -d -p 5000:5000 --restart=always --name registry registry:2

# A local GitLab runners can't pull from that registry because it will not have SSL. On a mac, you have to
# allow insecure registries. To do that, go to Docker Preferences > Docker Engine and add
# "insecure-registries" : ["gitlabdocker.sethdev:5000"] to the JSON
# Replace gitlabdocker.sethdev with your local development domain. 

# Update your /etc/hosts file with the development name ('gitlbdocker.sethdev') to point to your internal IP address, NOT your
# loopback address. The loopback address 127.0.0.1 will not be accessible from docker running from your runnder. 

# Pull dast image
docker pull registry.gitlab.com/gitlab-org/security-products/dast:latest

# Tag it
docker tag registry.gitlab.com/gitlab-org/security-products/dast:latest gitlabdocker.sethdev:5000/dast:latest

# Push it up
docker push gitlabdocker.sethdev:5000/dast:latest

# Check your work
curl gitlabdocker.sethdev:5000/v2/_catalog
curl -X GET http://gitlabdocker.sethdev:5000/v2/dast/tags/list

```

Another resource to checkout regarding setting up airgapped is Greg's https://gitlab.com/greg/airgap#simulate-air-gap
